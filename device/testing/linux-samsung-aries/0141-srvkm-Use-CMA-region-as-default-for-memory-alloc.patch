From a2539360f63ce12932de9db03ee72c2b6497e19a Mon Sep 17 00:00:00 2001
From: Gowtham Tammana <g-tammana@ti.com>
Date: Thu, 19 Sep 2019 16:31:50 -0500
Subject: [PATCH 141/170] srvkm: Use CMA region as default for memory alloc

If a CMA pool is setup for the device, use that as default for memory
allocation where we don't need kernel virtual mapping. The CMA pool is
setup in the kernel devicetree file.

Signed-off-by: Gowtham Tammana <g-tammana@ti.com>
---
 .../eurasia_km/services4/srvkm/env/linux/mm.c     | 15 +++++++++++++++
 .../eurasia_km/services4/srvkm/env/linux/mm.h     |  2 ++
 .../eurasia_km/services4/srvkm/env/linux/osfunc.c | 12 ++++++++++--
 .../services4/srvkm/env/linux/pvr_drm.c           |  4 ++++
 4 files changed, 31 insertions(+), 2 deletions(-)

diff --git a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.c b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.c
index da11a5633e7e..9a93ef0c1228 100644
--- a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.c
+++ b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.c
@@ -108,6 +108,8 @@ extern struct platform_device *gpsPVRLDMDev;
  */
 static atomic_t g_sPagePoolEntryCount = ATOMIC_INIT(0);
 
+static IMG_BOOL bCmaAllocation = IMG_FALSE;
+
 #if defined(DEBUG_LINUX_MEMORY_ALLOCATIONS)
 typedef enum {
     DEBUG_MEM_ALLOC_TYPE_KMALLOC = 0,
@@ -1511,6 +1513,10 @@ NewAllocCmaLinuxMemArea(IMG_SIZE_T uBytes, IMG_UINT32 ui32AreaFlags)
     dma_addr_t phys;
     void *cookie;
 
+    /* return if there is no device specific cma pool */
+    if (!bCmaAllocation)
+        return NULL;
+
     psLinuxMemArea = LinuxMemAreaStructAlloc();
     if (!psLinuxMemArea)
     {
@@ -2844,3 +2850,12 @@ LinuxMMInit(IMG_VOID)
     return PVRSRV_ERROR_OUT_OF_MEMORY;
 }
 
+IMG_VOID LinuxSetCMARegion(IMG_BOOL bCma)
+{
+    bCmaAllocation = bCma;
+
+    if (bCma)
+    {
+        PVR_TRACE(("%s:, CMA pool is setup for GPU\n", __FUNCTION__));
+    }
+}
diff --git a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.h b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.h
index 5963ae16d714..67c4821c4c5b 100644
--- a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.h
+++ b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/mm.h
@@ -681,5 +681,7 @@ const IMG_CHAR *LinuxMemAreaTypeToString(LINUX_MEM_AREA_TYPE eMemAreaType);
 const IMG_CHAR *HAPFlagsToString(IMG_UINT32 ui32Flags);
 #endif
 
+IMG_VOID LinuxSetCMARegion(IMG_BOOL bCma);
+
 #endif /* __IMG_LINUX_MM_H__ */
 
diff --git a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/osfunc.c b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/osfunc.c
index 3bb776f5c4c2..3e6d2726f0bd 100644
--- a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/osfunc.c
+++ b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/osfunc.c
@@ -222,7 +222,7 @@ OSAllocPages_Impl(IMG_UINT32 ui32AllocFlags,
 				  IMG_VOID **ppvCpuVAddr,
 				  IMG_HANDLE *phOSMemHandle)
 {
-    LinuxMemArea *psLinuxMemArea;
+    LinuxMemArea *psLinuxMemArea = IMG_NULL;
 
     PVR_UNREFERENCED_PARAMETER(ui32PageSize);
 
@@ -252,7 +252,15 @@ OSAllocPages_Impl(IMG_UINT32 ui32AllocFlags,
             /* Currently PVRSRV_HAP_SINGLE_PROCESS implies that we dont need a
              * kernel virtual mapping, but will need a user space virtual mapping */
 
-            psLinuxMemArea = NewAllocPagesLinuxMemArea(uiSize, ui32AllocFlags);
+            psLinuxMemArea = NewAllocCmaLinuxMemArea(uiSize, ui32AllocFlags);
+            if (!psLinuxMemArea)
+            {
+#if defined(DEBUG_LINUX_MEM_AREAS)
+                dev_err(&gpsPVRLDMDev->dev, "CMA region is either exhausted \
+                        or not present\n");
+#endif
+                psLinuxMemArea = NewAllocPagesLinuxMemArea(uiSize, ui32AllocFlags);
+            }
             if(!psLinuxMemArea)
             {
                 return PVRSRV_ERROR_OUT_OF_MEMORY;
diff --git a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/pvr_drm.c b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/pvr_drm.c
index 5f3600ee7dbc..f2c0d34548a6 100644
--- a/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/pvr_drm.c
+++ b/drivers/gpu/drm/pvrsgx/1.17.4948957/eurasia_km/services4/srvkm/env/linux/pvr_drm.c
@@ -62,6 +62,7 @@ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 #include <asm/ioctl.h>
 #include <drm/drmP.h>
 #include <drm/drm.h>
+#include <linux/of_reserved_mem.h>
 
 #include "img_defs.h"
 #include "services.h"
@@ -717,6 +718,9 @@ PVRSRVDrmProbe(struct platform_device *pDevice)
 	}
 #endif
 
+	LinuxSetCMARegion(of_reserved_mem_device_init(&pDevice->dev) ?
+			IMG_FALSE: IMG_TRUE);
+
 #if defined(PVR_NEW_STYLE_DRM_PLATFORM_DEV)
 	gpsPVRLDMDev = pDevice;
 
-- 
2.20.1

